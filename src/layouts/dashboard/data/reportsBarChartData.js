/**
=========================================================
* Material Dashboard 2 React - v2.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

export default {
  labels: ["Occupied Room", "Group Rooms", "Transient Rooms"],
  datasets: [
    {
      type: "bar",
      label: "OCC%",
      data: [22.8, 14.05, 8.75],
      backgroundColor: "#ffffff",
    },
    {
      type: "line",
      label: "ADR",
      data: [57.59, 58.17, 56.66],
      backgroundColor: "red",
      borderColor: "red",
    },
  ],
};
